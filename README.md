# ZIP_code_finder

_Author: Leonardo Abundis - <leonardo.abundis.mos@gmail.com>_

This is a simple German ZIP Code search application. This App will help you to find the ZIP code of your current location by typing the GPS coordinates or with the help of Google Maps. The app is composed with a JS Frontend (React), a RESTful API in the backend  (Express JS) and a DB backend (MongoDB).

## How to run the app?

There are two options to run the app:

### Online

The app is already mounted and running in an EC2 service on AWS, to open the app you only need to visit the following link: http://ec2-54-236-45-143.compute-1.amazonaws.com:3000/

### Local

Run the app in your local computer 
1. Download the zip folder of the repository and unzip it to your preferred directory
2. Install `node.js` and `npm` or verify if you already have installed these packages ( https://nodejs.org/es/download/ )
3. Open a terminal and change to the directory **zip_code_finder** with the command `cd your_path/zip_code_finder`
4. Run the command `npm start` to run the app in your local environment
5. Click on the following link to access the app: http://localhost:3000/ 

## How does the app works?

The app is very intuitive and easy to use, but you can check the guide below for reference:

### 1. Login
Login in the app with these credentials:
```
Username: SachControl
Password: 12345
```
<img src="/example/Login.png" alt="drawing" width="75%"/>

### 2. Home
In the home page, you can read a description of the app and find the link for this repository and the link for my personal website
- In the top of the page there is a bar where you can choose to find the coordinates by GPS coordinastes or by a pin in a map
- There is a logout button in the top of the home page
<img src="/example/Home.png" alt="drawing" width="75%"/>

### 3. Coordinates
In the coordinates page, you need to type the GPS coordinates of any location in Germany, then click on send and the app will return the ZIP code and the city of that location
- There is a logout button in the top of the home page
- In the `example directory` there is a file called `test_data` with 10 coordinates to test
- Example of how you need to type the data:
```
Longitute: 51.078001
Latitude: 13.696318
```
<img src="/example/Coordinates.png" alt="drawing" width="75%"/>

### 4. Map
In the map page, you need to click on any location of the map and the pin will change to that position, then the app will return he ZIP code and the city of that pin
- There is a logout button in the top of the home page
- The interaction with the map is the same as with any other google map

<img src="/example/Map.png" alt="drawing" width="75%"/>

_Note: The app is also supported in mobile, the only difference is that the top menu will be in a side bar_

## How is the app made?
### Dev environment
A development environment was established with the help of webpack, babel and react
### Database
The database is mounted in a cloud version of **MongoDB** called Atlas.<br><br>
Steps followed to upload the data to MongoDB:
1. Download the GeoJSON data from this link: https://www.suche-postleitzahl.org
2. Transform the data GeoJSON to JSON and upload it to MongoDB using the Python_GeoJSON_JSON files
    - These auxiliary files were downloaded from this repo: https://github.com/rtbigdata/geojson-mongo-import.py
    - These auxiliary files were edited to match this project requirements 
    - Coomand to run the python code<br>

`python geojson-mongo-import-atlas.py -f points.geojson -s <route> -port <port> -d <database> -c <collection>`<br><br>
_Note: Change the url inside the `geojson-mongo-import-atlas.py` file_
### Backend
The backend of the project is handled by **Express.js.** 
- This tool handles the RESTful API
    - For the login process
    - For the app request to the database
- This tool handles the queries to the database
- Query to obtain the ZIP code and city by passing the coordinates
```json
{ "geometry": { "$geoIntersects": { "$geometry": { "type": "Point", "coordinates": [req.params.lat, req.params.lon] } } } }
```
### Frontend
The frontend of the tool is handled by **React.js**.
- The app has several routes and components
- This tool handles the routes redirection and the rendering of all the web page
### Cloud
The app was mounted in an **EC2** service on **AWS**, this makes the app available online.

## Future improvements

### Registry
The registry of new users, right now the app only has one user in the registry. Step to do this:
1. Create a new database
2. New route and component for the registry page
3. Create a new RESTful API which POST to this new database
### GUI
Make the GUI more user friendly and create new features 
### Backend
Change the backend to another technology like .Net Core 
