import React, { Component, useState } from 'react';
import Home from './Home';
import Coordinates from './Coordinates';
import MapGoogle from './Map';
import Login from './Login';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
    const [token, setToken] = useState();

    if (!token) {
        return <Login setToken={setToken} />
    }

    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path="/"><Home /></Route>
                    <Route exact path="/coordinates"><Coordinates /></Route>
                    <Route exact path="/map"><MapGoogle /></Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;