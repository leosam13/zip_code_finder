import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Coordinates extends Component {

    constructor() {
        super();
        this.state = {
            longitude: '',
            latitude: '',
            place: ''
        };
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    addTask(e) {
        fetch(`/api/tasks/${this.state.longitude}/${this.state.latitude}`)
            .then(res => res.json())
            .then(data => {
                this.setState({ place: data[0].properties.note });
            });
        e.preventDefault();
    }

    componentDidMount() {
        this.fetchTasks();
        let sidenav = document.querySelector("#mobile-links");
        M.Sidenav.init(sidenav, {});
    }

    fetchTasks() {
        fetch('/api/tasks/50.891497/14.803171')
            .then(res => res.json())
            .then(data => {
                console.log("Coordinates ready");
            });
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div>
                {/* NAVIGATION */}
                <div className="navbar-fixed">
                    <nav className="nav-wrapper light-blue darken-4">
                        <div className="container">
                            <a href="#" className="brand-logo">ZIP-Finder</a>
                            <a href="#" className="sidenav-trigger" data-target="mobile-links">
                                <i className="material-icons">menu</i>
                            </a>
                            <ul className="right hide-on-med-and-down">
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/coordinates">Coordinates</Link></li>
                                <li><Link to="/map">Map</Link></li>
                                <li><a href="" className="btn white indigo-text">Logout</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <ul className="sidenav" className="sidenav sidenav-close" id="mobile-links">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/coordinates">Coordinates</Link></li>
                    <li><Link to="/map">Map</Link></li>
                    <li><a href="" className="btn white light-blue darken-4">Logout</a></li>
                </ul>

                <div className="container">
                    <div className="row" >
                        <div className="col s5">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addTask}>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input name="longitude" onChange={this.handleChange} type="text" placeholder="Longitude" value={this.state.longitude} />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <textarea name="latitude" onChange={this.handleChange} placeholder="Latitude" className="materialize-textarea" value={this.state.latitude}></textarea>
                                            </div>
                                        </div>
                                        <button className="btn light-blue darken-4">
                                            Send
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col s7">
                            <table>
                                <thead>
                                    <tr>
                                        <th>ZIP code and City</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{this.state.place}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <center>
                    <p style={{
                        position: 'relative',
                        bottom: '0',
                        width: '100%',
                        height: '60px'
                    }}>&copy; Copyright 2021 Leonardo Abundis</p>
                </center>
            </div>
        )
    }
}

export default Coordinates;