import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
    componentDidMount() {
        let sidenav = document.querySelector("#mobile-links");
        M.Sidenav.init(sidenav, {});
    }

    render() {
        return (
            <div>
                {/* NAVIGATION */}
                <div className="navbar-fixed">
                    <nav className="nav-wrapper light-blue darken-4">
                        <div className="container">
                            <a href="#" className="brand-logo">ZIP-Finder</a>
                            <a href="#" className="sidenav-trigger" data-target="mobile-links">
                                <i className="material-icons">menu</i>
                            </a>
                            <ul className="right hide-on-med-and-down">
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/coordinates">Coordinates</Link></li>
                                <li><Link to="/map">Map</Link></li>
                                <li><a href="" className="btn white indigo-text">Logout</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <ul className="sidenav" className="sidenav sidenav-close" id="mobile-links">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/coordinates">Coordinates</Link></li>
                    <li><Link to="/map">Map</Link></li>
                    <li><a href="" className="btn white light-blue darken-4">Logout</a></li>
                </ul>

                <div className="container">
                    <div className="row"></div>
                    <center>
                        <div className="row">
                            <div className="col s12 m10 offset-m1">
                                <div className="card">
                                    <div className="card-content">
                                        <h3>Welcome to the best German ZIP code Finder! </h3>
                                        <img src="./images/Flag_map_of_Germany.png" alt="flag" style={{
                                            "width": "25%",
                                            "height": "25%"
                                        }}></img>
                                        <br></br>
                                        <br></br>
                                        <p>This is a simple German ZIP Code search application. The app is
                                            composed of a JS Frontend (React), a RESTful API in the backend
                                            (Express JS) and a DB backend (MongoDB).</p>
                                        <br></br>
                                        <p>You can choose between 2 options to find the ZIP code of your German Location:</p>
                                        <ul style={{
                                            "listStyleType": "disc",
                                            "listStylePosition": "inside"
                                        }}>
                                            <li style={{
                                                "listStyleType": "disc",
                                                "listStylePosition": "inside"
                                            }}>Set the GPS coordinates manually</li>
                                            <li style={{
                                                "listStyleType": "disc",
                                                "listStylePosition": "inside"
                                            }}>Chooose a location in Google Maps</li>
                                        </ul>
                                        <p>Visit my GitLab for the documentation: <a href='https://gitlab.com/leosam13/zip_code_finder'>https://gitlab.com/leosam13/zip_code_finder</a></p>
                                        <p>Visit my website to contact me: <a href='http://lsamcv.s3-website-us-east-1.amazonaws.com/'>http://lsamcv.s3-website-us-east-1.amazonaws.com/</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>

                <center>
                    <p style={{
                        position: 'relative',
                        bottom: '0',
                        width: '100%',
                        height: '60px'
                    }}>&copy; Copyright 2021 Leonardo Abundis</p>
                </center>
            </div>
        );
    }
}
export default Home;