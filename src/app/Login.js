import React, { useState } from 'react';
import PropTypes from 'prop-types';
var md5 = require('md5');


const user1 = "SachControl";
const password1 = "827ccb0eea8a706c4c34a16891f84e7b";

async function loginUser(credentials) {
    return fetch('http://localhost:3000/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    })
        .then(data => data.json())
}


export default function Login({ setToken }) {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = e => {
        if (username == user1 && password1 == password) {
            e.preventDefault();
            const token = loginUser({
                username,
                password
            });
            setToken(token);
            M.toast({html: 'Welcome back!', classes: 'light-blue darken-4'});
        }
        else{
            alert("You have entered an invalid username or password");
        }
    }

    return (

        <div>
            <div className="navbar-fixed">
                <nav className="nav-wrapper light-blue darken-4">
                    <a href="#" className="brand-logo center">ZIP-Finder</a>
                </nav>
            </div>
            <div className="container">
                <div className="row"></div>
                <div className="row"></div>
                <center>
                    <div className="row">
                        <div className="col s12 m4 offset-m4">
                            <div className="card">
                                <div className="card-content">
                                    <form id="form1" action="http://localhost:3000/login" onSubmit={handleSubmit} method="POST" target="hiddenFrame">
                                        <h5>Login into your account!</h5>
                                        <div className="row"></div>
                                        <input name="username" onChange={e => setUserName(e.target.value)} type="text" placeholder="User" />
                                        <div className="row"></div>
                                        <input name="password" onChange={e => setPassword(md5(e.target.value))} type="password" placeholder="Password" />
                                        <div className="row"></div>
                                        <button type="submit" className="btn light-blue darken-4" form="form1">Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </center>
            </div>
            <center>
                <p style={{
                    position: 'relative',
                    bottom: '0',
                    width: '100%',
                    height: '60px'
                }}>&copy; Copyright 2021 Leonardo Abundis</p>
            </center>
        </div>
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}

