import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

class MapGoogle extends Component {

    constructor() {
        super();
        this.state = {
            longitude: '51.077958',
            latitude: '13.696279',
            place: '',
            position: {
                lat: '13.696279',
                lng: '51.077958'
            }
        };
        this.handleMapClick = this.handleMapClick.bind(this);
    }

    handleMapClick(ref, map, e) {
        let location = this.state.position;
        location.lat = e.latLng.lat();
        location.lng = e.latLng.lng();

        this.setState({
            position: location
        })

        fetch(`/api/tasks/${this.state.position.lat}/${this.state.position.lng}`)
            .then(res => res.json())
            .then(data => {
                this.setState({ place: data[0].properties.note });
            });

    };

    componentDidMount() {
        console.log("Map ready");
        this.fetchTasks();
        let sidenav = document.querySelector("#mobile-links");
        M.Sidenav.init(sidenav, {});
    }

    fetchTasks() {

        fetch(`/api/tasks/${this.state.longitude}/${this.state.latitude}`)
            .then(res => res.json())
            .then(data => {
                this.setState({ place: data[0].properties.note });
            });
    }

    render() {
        return (

            <div>
                {/* NAVIGATION */}
                <div className="navbar-fixed">
                    <nav className="nav-wrapper light-blue darken-4">
                        <div className="container">
                            <a href="#" className="brand-logo">ZIP-Finder</a>
                            <a href="#" className="sidenav-trigger" data-target="mobile-links">
                                <i className="material-icons">menu</i>
                            </a>
                            <ul className="right hide-on-med-and-down">
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/coordinates">Coordinates</Link></li>
                                <li><Link to="/map">Map</Link></li>
                                <li><a href="" className="btn white indigo-text">Logout</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <ul className="sidenav" className="sidenav sidenav-close" id="mobile-links">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/coordinates">Coordinates</Link></li>
                    <li><Link to="/map">Map</Link></li>
                    <li><a href="" className="btn white light-blue darken-4">Logout</a></li>
                </ul>
                <Map
                    google={this.props.google}
                    style={{
                        width: '49%',
                        height: '70%',
                        position: 'absolute',
                        marginTop: '10px',
                        marginLeft: '10px'
                    }}
                    initialCenter={{
                        lat: 51.077958,
                        lng: 13.696279
                    }}
                    zoom={10}
                    onClick={this.handleMapClick}
                >

                    <Marker position={{ lat: this.state.position.lat, lng: this.state.position.lng }} name={'Current location'} />

                </Map>
                <div className="row" style={{
                    'minHeight': 'calc(100vh - 160px)'
                }}>
                    <div className="col s6"></div>
                    <div className="col s4">
                        <table>
                            <thead>
                                <tr>
                                    <th className="left-align">ZIP code and City</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="left-align">{this.state.place}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col s2"></div>
                </div>
                <center>
                    <p style={{
                        position: 'static',
                        bottom: '0',
                        width: '100%',
                        height: '60px'
                    }}>&copy; Copyright 2021 Leonardo Abundis</p>
                </center>
            </div>

        )
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyCBrRquWw_Ol6z9Zw8DtV5AmYuRd7f5Upc")
})(MapGoogle)