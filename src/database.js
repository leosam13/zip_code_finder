const mongoose = require('mongoose');

const URI = 'mongodb+srv://lsam:lsam1234@zipcodecloud.tle5o.mongodb.net/ZIP_code_db?retryWrites=true&w=majority';

mongoose.connect(URI)
    .then(db => console.log('DB is connected'))
    .catch (err => console.error(err));

module.exports = mongoose;