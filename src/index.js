const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');

const { mongoose } = require('./database');

const app = express();

//Config
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(cors());

//Routes
app.use('/api/tasks', require('./routes/task.routes'));
app.use('/login', (req, res) => {
    res.send({
        token: 'test123'
    });
});


//Static files
app.use(express.static(path.join(__dirname, 'public')));
app.get('*', function (req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, 'public') });
});

//Starting the server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
});