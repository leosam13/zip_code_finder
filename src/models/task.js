const mongoose = require('mongoose');
const { Schema } = mongoose;

const TaskSchema = new Schema({
    _id: { type: String},
    type: {type: String},
    geometry: {
        type: {type: String},
        coordinates:[Number]
    },
    properties:{
        plz: {type: String},
        note:{type: String},
        qkm:{type: String},
        einwohner:{type: String}
    }
},
{collection: 'sample'});

module.exports = mongoose.model('Task', TaskSchema);