const express = require('express');
const router = express.Router();

const Task = require('../models/task')

router.get('/:lon/:lat', async (req, res) => {
    const tasks = await Task.find({ "geometry": { "$geoIntersects": { "$geometry": { "type": "Point", "coordinates": [req.params.lat, req.params.lon] } } } });
    res.json(tasks);
});

module.exports = router;